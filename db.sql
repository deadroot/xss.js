CREATE TABLE `xssprobe` (
  `id` int(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `realIP` text,
  `browser` text,
  `ua` text,
  `os` text,
  `lang` text,
  `referrer` text,
  `location` text,
  `toplocation` text,
  `cookie` text,
  `domain` text,
  `title` text,
  `cpucores` text,
  `body` text,
  `sessionStorage` text,
  `localStorage` text,
  `serverHeaders` text,
  `screen` text,
  `flash` text,
  `loginData` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `xssprobe`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `xssprobe`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;